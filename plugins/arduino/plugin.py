# Communication with arduino to control blinds 
# over USB simple serie link
# Author: Adrien Jo 
#
"""
<plugin key="ArduinoComm" name="Arduino Comm USB" author="Adri1Jo" version="0.0.1">
    <params>
        <param field="SerialPort" label="Serial port" width="150px" required="true" default="/dev/ttyACM0"/>
        <param field="Mode1" label="volet" width="150px">
            <options>
                <option label="buanderie" value="buanderie" default="true"/>
                <option label="cuisine" value="cuisine"/>
                <option label="Baie Ouest" value="baieOuest"/>
                <option label="Baie Sud" value="baieSud"/>
                <option label="Fixe" value="fixe"/>
                <option label="Chambre enfants" value="bureau"/>
                <option label="Chambre Bebe" value="chBebe"/>
                <option label="Chambre Tilio" value="chTilio"/>
                <option label="Chambre Parents" value="chParents"/>
                <option label="Garage Haut" value="GHaut"/>
                <option label="Garage Bas" value="GBas"/>
                <option label="VMC Marche Force" value="VMC"/>
            </options>
        </param>
    </params>
</plugin>
"""
import Domoticz
import json

global deviceName

class serialLink:
    enabled = False
    def __init__(self):
        return

    def onStart(self):
        global deviceName
        deviceName = ""

        self.link = Domoticz.Connection(Name="Serial Connection", Transport="Serial", Protocol="line", Address=Parameters["SerialPort"], Baud=19200)
        self.link.Connect()
        
        if (Parameters["Mode1"] == "buanderie"):
            deviceName = "Buanderie"
        elif (Parameters["Mode1"] == "cuisine"):
            deviceName = "Cuisine"
        elif (Parameters["Mode1"] == "baieOuest"):
            deviceName = "Baie Ouest"
        elif (Parameters["Mode1"] == "baieSud"):
            deviceName = "Baie Sud"
        elif (Parameters["Mode1"] == "fixe"):
            deviceName = "Fixe"
        elif (Parameters["Mode1"] == "bureau"):
            deviceName = "Bureau"
        elif (Parameters["Mode1"] == "chBebe"):
            deviceName = "Chambre Bebe"
        elif (Parameters["Mode1"] == "chTilio"):
            deviceName = "Chambre Tilio"
        elif (Parameters["Mode1"] == "chParents"):
            deviceName = "Chambre Parents"
        elif (Parameters["Mode1"] == "GHaut"):
            deviceName = "Garage Haut"
        elif (Parameters["Mode1"] == "GBas"):
            deviceName = "Garage Bas"
        elif (Parameters["Mode1"] == "VMC"):
            deviceName = "VMC"
        else:
            Domoticz.Log("Can't fine the blind requested")
        Domoticz.Log("devices " + str(Devices))
        
        if (len(Devices) == 0):
            Domoticz.Device(Name=deviceName, TypeName="Switch" ,Unit=1).Create()

    def onStop(self):
        Domoticz.Log("onStop called")

    def onConnect(self, Connection, Status, Description):
        if (Status == 0):
            Domoticz.Log("Connection sucessful to: " + Parameters["SerialPort"])
        else:
            Domoticz.Log("Connection FAIL to: " + Parameters["SerialPort"] + "Error code: " + str(Status))


    def onCommand(self, Unit, Command, Level, Hue):
        global deviceName
#        Domoticz.Log("onCommand called for Unit " + str(Unit) + ": Parameter '" + str(Command) + "', Level: " + str(Level))
        command = {}
        Domoticz.Log("Comand= " + Command + " DeviceName = " + deviceName)
        if (deviceName == 'VMC'):
            if (Command == 'Off'):
                command = {
                        "type":"command",
                        "group":"vmc",
                        "action":"stop"
                        }
            else:
                command = {
                        "type":"command",
                        "group":"vmc",
                        "action":"start"
                        }
            Domoticz.Log("VMC")
        
        elif (deviceName.find("Garage") != -1):
            command = {
                    "type":"command",
                    "group":"garage",
                    "id":deviceName
                    } 
            Domoticz.Log("Garage")

        elif (Command == 'Off'):
            command = {
                    "type":"command",
                    "group":"blind",
                    "id":deviceName,
                    "action":"up"
                    }
            Domoticz.Log("up the blind")
        else:
            command = {
                    "type":"command",
                    "group":"blind",
                    "id":deviceName,
                    "action":"down"
                    }
            Domoticz.Log("down the blind")
        
        self.link.Send(json.dumps(command) + "#")
    

    def onNotification(self, Name, Subject, Text, Status, Priority, Sound, ImageFile):
        Domoticz.Log("Notification: " + Name + "," + Subject + "," + Text + "," + Status + "," + str(Priority) + "," + Sound + "," + ImageFile)

    def onDisconnect(self, Connection):
        Domoticz.Log("onDisconnect called")

    def onHeartbeat(self):
        return True

global _plugin
_plugin = serialLink()

def onStart():
    global _plugin
    _plugin.onStart()

def onStop():
    global _plugin
    _plugin.onStop()

def onConnect(Connection, Status, Description):
    global _plugin
    _plugin.onConnect(Connection, Status, Description)

def onMessage(Connection, Data, Status, Extra):
    global _plugin

def onCommand(Unit, Command, Level, Hue):
    global _plugin
    _plugin.onCommand(Unit, Command, Level, Hue)

def onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile):
    global _plugin
    _plugin.onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile)

def onDisconnect(Connection):
    global _plugin
    _plugin.onDisconnect(Connection)

def onHeartbeat():
    global _plugin
    _plugin.onHeartbeat()

    # Generic helper functions
def DumpConfigToLog():
    for x in Parameters:
        if Parameters[x] != "":
            Domoticz.Debug( "'" + x + "':'" + str(Parameters[x]) + "'")
    Domoticz.Debug("Device count: " + str(len(Devices)))
    for x in Devices:
        Domoticz.Debug("Device:           " + str(x) + " - " + str(Devices[x]))
        Domoticz.Debug("Device ID:       '" + str(Devices[x].ID) + "'")
        Domoticz.Debug("Device Name:     '" + Devices[x].Name + "'")
        Domoticz.Debug("Device nValue:    " + str(Devices[x].nValue))
        Domoticz.Debug("Device sValue:   '" + Devices[x].sValue + "'")
        Domoticz.Debug("Device LastLevel: " + str(Devices[x].LastLevel))
    return
