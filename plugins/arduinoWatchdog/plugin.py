# Communication with arduino to control blinds 
# over USB simple serie link
# Author: Adrien Jo 
#
"""
<plugin key="ArduinoWatchdog" name="Arduino Watchdog" author="Adri1Jo" version="0.0.1">
    <params>
        <param field="SerialPort" label="Serial port" width="150px" required="true" default="/dev/ttyACM0"/>
    </params>
</plugin>
"""
import Domoticz
import json

class serialLink:
    enabled = False
    watchdog = False
    watchdogCommandCnt = 0
    commandTimer = 0
    counter = 0
    dataLocal = ""
    link = {}

    def __init__(self):
        return

    def onStart(self):
        Domoticz.Log("onStart called")
        self.link = Domoticz.Connection(Name="Serial Connection", Transport="Serial", Protocol="line", Address=Parameters["SerialPort"], Baud=19200)
        self.link.Connect()
        
    def onStop(self):
        Domoticz.Log("onStop called")

    def onConnect(self, Connection, Status, Description):
        Domoticz.Log("onConnect called")

    def onMessage(self, Connection, Data, Status, Extra):

        Domoticz.Log("Connection: " + str(Connection) + "/ Status" + str(Status))
        try:
            if (str(Data) != ""):
                #jsonData = json.loads(Data)
                #Domoticz.Log("Json received: " + json.dumps(jsonData))
                # read data in JSON
                #val = jsonData['toto']
                received = str(Data)
                received = received.replace("b\'", "")
                received = received.replace("\'", "")
                self.dataLocal = self.dataLocal + received
                Domoticz.Log("Total val: " + self.dataLocal)
                if ("#" in self.dataLocal and self.dataLocal.endswith("#\'")):
                    array = str(self.dataLocal).split("#")
                    #Domoticz.Log("data splited: " + str(array))
                    for val in array:
                        if (val.find("42") != -1):
                            self.watchdog = False
                            self.counter = 0

                if (not self.dataLocal.endswith("#\'")):
                    array = str(self.dataLocal).split("#")
                    self.dataLocal = array[-1]
                else:
                    self.dataLocal = ""
        except:
            Domoticz.Log("Fail to read JSON value")
    
    def onNotification(self, Name, Subject, Text, Status, Priority, Sound, ImageFile):
        Domoticz.Log("Notification: " + Name + "," + Subject + "," + Text + "," + Status + "," + str(Priority) + "," + Sound + "," + ImageFile)

    def onDisconnect(self, Connection):
        Domoticz.Log("onDisconnect called")

    def onHeartbeat(self):
        
#        Domoticz.Log("On Heartbeat, c= " + str(self.counter))
        if (self.counter >= 12):
            if (self.watchdog  == True) and (self.counter > 25):
                # restart Arduino because it won't respond, recall the connection because Arduino restart on serial connection
                self.link = Domoticz.Connection(Name="Serial Connection", Transport="Serial", Protocol="line", Address=Parameters["SerialPort"], Baud=115200)
                self.link.Connect()
                Domoticz.Log("Reset ARDUINO")
                self.watchdog = False
                Domoticz.Log("counter = " + str(self.counter))
                self.dataLocal = ""
                self.counter = -1
            # send heartbeat command to Arduino => it should respond 4242
            command = {
                 "type":"command",
                 "group":"watchdog"
                 }
            self.link.Send(json.dumps(command)+'#')
            Domoticz.Log(json.dumps(command) + '#')
#           # Activate the watchdog
            self.watchdog = True
        self.counter += 1


global _plugin
_plugin = serialLink()

def onStart():
    global _plugin
    _plugin.onStart()

def onStop():
    global _plugin
    _plugin.onStop()

def onConnect(Connection, Status, Description):
    global _plugin
    _plugin.onConnect(Connection, Status, Description)

def onMessage(Connection, Data, Status, Extra):
    global _plugin
    _plugin.onMessage(Connection, Data, Status, Extra)

def onCommand(Unit, Command, Level, Hue):
    global _plugin

def onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile):
    global _plugin
    _plugin.onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile)

def onDisconnect(Connection):
    global _plugin
    _plugin.onDisconnect(Connection)

def onHeartbeat():
    global _plugin
    _plugin.onHeartbeat()

    # Generic helper functions
def DumpConfigToLog():
    for x in Parameters:
        if Parameters[x] != "":
            Domoticz.Debug( "'" + x + "':'" + str(Parameters[x]) + "'")
    Domoticz.Debug("Device count: " + str(len(Devices)))
    for x in Devices:
        Domoticz.Debug("Device:           " + str(x) + " - " + str(Devices[x]))
        Domoticz.Debug("Device ID:       '" + str(Devices[x].ID) + "'")
        Domoticz.Debug("Device Name:     '" + Devices[x].Name + "'")
        Domoticz.Debug("Device nValue:    " + str(Devices[x].nValue))
        Domoticz.Debug("Device sValue:   '" + Devices[x].sValue + "'")
        Domoticz.Debug("Device LastLevel: " + str(Devices[x].LastLevel))
    return
